# 4.1. Redo the cleaning processes as presented in the data cleaning notebook, that include:
#- Clean column names

#- Convert RAM, Price, Weight into numerical columns (create new columns)

#- Extract CPU speed from CPU column

#- Correcting values of os column


import numpy as np
import pandas as pd
laptops=pd.read_csv('laptops.csv', encoding='latin-1')
print(laptops)
#########################################################################
#4.1.1 Clean column names
#Function clean_label to clean the names of the columns
def clean_label(s):
    s=s.strip()# clean white space in the left and right of str
    s=s.replace("Operating System", "os")
    s=s.replace(" ", "_")
    s = s.replace("(", "")
    s = s.replace(")", "")
    s=s.lower()
    return s

new_label=[clean_label(column) for column in laptops.columns]
laptops.columns=new_label

print(laptops.columns)


######################################################################
#4.1.2 Convert RAM, Price, Weight into numerical columns
# (create new columns)

#convert ram
print(laptops.ram)

#remove the GB symbol by slicing each string not
# included the last two index
laptops.ram.str.slice(0,-2).astype('int')

laptops['ram_gb']=laptops.ram.str.slice(0,-2).astype('int')

print(laptops.ram_gb)

#convert Price
#clean the price_euro column
laptops['price_euros_num']=laptops.price_euros.str.replace(',','.').astype('float')
print(laptops.price_euros_num)

#convert Weight
laptops['weight_fixed']=laptops.weight.str.replace('kg','').str.replace('s','').astype('float')
print('convert Weight\n')
print(laptops.weight_fixed)

###########################################################
#4.1.3 Extract CPU speed from CPU column

#checking whether is there any cpu speed is written other than GHz
print('checking whether is there any cpu speed is written other than GHz\n')
laptops.cpu[~laptops.cpu.str.match(".*[0-9](GHz)$")]

laptops['cpu_speed_ghz']=laptops.cpu.str.split().apply(lambda x: x[-1]).str.replace("GHz", "")
print('cpu speed from cpu column\n')
print(laptops['cpu_speed_ghz'])

#4.1.4 Correcting values of os column

laptops.os.value_counts()
mapping = {"Windows": "Windows", "No OS": "No OS", "Linux": "Linux", "Chrome OS": "Chrome OS",
           "macOS": "macOS", "Mac OS": "macOS", "Android": "Android"}

#adding new column os_new
laptops['os_new']=laptops.os.map(mapping)

# How many null values in each column ?
laptops.isnull().sum()

#what are the os_version, dropna=False to get NaN
laptops.os_version.value_counts(dropna=False)

# What are the os of the laptops with null values in os_version ?
laptops.loc[laptops.os_version.isnull(), "os"].value_counts()

 #What are the os_version of laptop that run macOS ?
laptops.loc[laptops.os_new == "macOS", "os_version"].value_counts(dropna=False)

#start to really remove here
# Make a copy of laptops and modify values on the copy version
laptops_copy = laptops.copy()

#filling no os to os_version where os is No OS
laptops_copy.loc[laptops.os_new == "No OS", "os_version"] = "No OS"
# filling x to os_version where os is MacOS
laptops_copy.os_version.value_counts(dropna=False)

# Deleting the rows which contain null
laptops_copy.dropna(inplace=True)

## Attention: Remove all columns with null values
#laptops.dropna(axis=1)

#checking os_version
print(laptops_copy.os_version)

#what are the os_version, dropna=False to get NaN
print('what are the os_version after converting\n')
print(laptops_copy.os_version.value_counts(dropna=False))


