#4.3/ Extract Screen Resolution (for example 2880x1800), and answer:
#- How many laptop has full hd (1920x1080) screen ?
#- What is the popular screen resolution of Macbook ?
#################################################################

######################################################################
import numpy as np
import pandas as pd



############first we convert the columns' name############################
laptops=pd.read_csv('laptops.csv', encoding='latin-1')

#Function clean_label to clean the names of the columns
def clean_label(s):
    s=s.strip()# clean white space in the left and right of str
    s=s.replace("Operating System", "os")
    s=s.replace(" ", "_")
    s = s.replace("(", "")
    s = s.replace(")", "")
    s=s.lower()
    return s

new_label=[clean_label(column) for column in laptops.columns]
laptops.columns=new_label
##########################################################################################

#4.3.1 Extract Screen Resolution (for example 2880x1800)
laptops['screen_resolution']=laptops.screen.str.split().apply(lambda x: x[-1])

print('the first five screen_resolution are\n',laptops.screen_resolution.head())

# How many laptop has full hd (1920x1080) screen ?
print('the number of laptops which have full hd (1920x1080) screen is \n',laptops.screen_resolution.value_counts()['1920x1080'])



###### 4.3.3 What is the popular screen resolution of Macbook ?
#########################################################

# Correcting values of os column
###########first we convert os to os_new#######################
laptops.os.value_counts()
mapping = {"Windows": "Windows", "No OS": "No OS", "Linux": "Linux", "Chrome OS": "Chrome OS",
           "macOS": "macOS", "Mac OS": "macOS", "Android": "Android"}

#adding new column os_new
laptops['os_new']=laptops.os.map(mapping)

####
#convert screen size column into numeric
laptops['screen_size_num']=laptops.screen_size.str.slice(0,-1).astype('float')
print('screen_size_num is\n',laptops['screen_size_num'])
####################
print('the popular screen resolution of Macbook is\n')
print(laptops.loc[laptops.os_new =='macOS','screen_size_num'].value_counts())
###########################################################