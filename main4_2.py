#4.2/ Convert Screen Size column in to numeric and use that column to answer:
#- What is the popular screen size ?
#- What is the minimum screen size of Ultrabook ?
#- What is the average screen size of gaming laptop ?
######################################################################
import numpy as np
import pandas as pd



############first we convert the columns' name############################
laptops=pd.read_csv('laptops.csv', encoding='latin-1')

#Function clean_label to clean the names of the columns
def clean_label(s):
    s=s.strip()# clean white space in the left and right of str
    s=s.replace("Operating System", "os")
    s=s.replace(" ", "_")
    s = s.replace("(", "")
    s = s.replace(")", "")
    s=s.lower()
    return s

new_label=[clean_label(column) for column in laptops.columns]
laptops.columns=new_label
##########################################################################################


#convert screen size column into numeric
laptops['screen_size_num']=laptops.screen_size.str.slice(0,-1).astype('float')
print('screen_size_num is\n',laptops['screen_size_num'])

# the popular screen size
laptops.screen_size_num.value_counts()
# the most popular screen size
print('The most popular screen size is\n',laptops.screen_size_num.value_counts().idxmax())

#- What is the minimum screen size of Ultrabook ?
print('the minimum screen size of Ultrabook is \n',laptops.loc[laptops.category == "Ultrabook", "screen_size_num"].value_counts().idxmin())

#- What is the average screen size of gaming laptop ?
print('the average screen size of gaming laptops is\n',laptops.loc[laptops.category=='Gaming','screen_size_num'].mean())