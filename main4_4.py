#4.4/ Using column storage to create 3 new columns named: storage_gb, storage_ssd, storage_hdd.
# #storage_gb stores the volume of the storage. storage_ssd indicates whether a storage is ssd;
#  values of storage_ssd is either 0 or 1 where 1 means the storage device is ssd. storage_hdd
# indicates whether a storage is hdd; values of storage_hdd is either 0 or 1 where 1 means the storage
# device is hdd. Use this column to answer:
#- How many laptop have ssd storage ? How many laptop have hdd storage ?
#- On a same plot, plot a histogram for the volume of ssd storage and a histogram for the volume
# of hdd storage.



######################################################################
import numpy as np
import pandas as pd



############first we convert the columns' name############################
laptops=pd.read_csv('laptops.csv', encoding='latin-1')

#Function clean_label to clean the names of the columns
def clean_label(s):
    s=s.strip()# clean white space in the left and right of str
    s=s.replace("Operating System", "os")
    s=s.replace(" ", "_")
    s = s.replace("(", "")
    s = s.replace(")", "")
    s=s.lower()
    return s

new_label=[clean_label(column) for column in laptops.columns]
laptops.columns=new_label
##########################################################################################

############################################
#4.4.1 Using column storage to create 3 new columns named: storage_gb, storage_ssd, storage_hdd.
# #storage_gb stores the volume of the storage. storage_ssd indicates whether a storage is ssd;
#  values of storage_ssd is either 0 or 1 where 1 means the storage device is ssd. storage_hdd
# indicates whether a storage is hdd; values of storage_hdd is either 0 or 1 where 1 means the storage
# device is hdd.